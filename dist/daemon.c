#include <stdio.h>
//#include <mysql.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <openssl/md5.h>
#include <curl/curl.h>
#include <sys/stat.h>

char DOMAIN[]="net.nsysu.edu.tw";
char SERVER[]="net.nsysu.edu.tw";
char nip[20],rip[20];
char hostname[20];

functionST(void *ptr,size_t size, size_t nmemb, void *stream) {
    strcpy(hostname,ptr);
    //printf("RES:%s",hostname);
}

int serverAction(char *cuser, char *cps, char *ip)
{
    char url[150];
    if(ip==NULL)
        sprintf(url,"http://dns.net.nsysu.edu.tw/ccurl.php?u=%s&p=%s",cuser,cps);
    else
        sprintf(url,"http://dns.net.nsysu.edu.tw/ccurl.php?u=%s&p=%s&ip=%s",cuser,cps,ip);
    CURL *curl;
    CURLcode res;

    curl=curl_easy_init();
    if(curl)
    {
        curl_easy_setopt(curl, CURLOPT_URL, url);
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, functionST);
        curl_easy_perform(curl);
        curl_easy_cleanup(curl);
    }
    if(strcmp(hostname,"ERR0")==0) {
        printf("Database connection failed!\n");
        exit(1);
    }
    if(strcmp(hostname,"ERR1")==0 || strcmp(hostname,"ERR2")==0) {
        printf("Username or password not correct!\n");
        exit(1);
    }
    if(strcmp(hostname,"ERR3")==0) {
        printf("DNS update failed!\n");
        exit(1);
    }

}

int login()
{
    char cuser[20], *cps,md5ps[33];
    unsigned char digest[MD5_DIGEST_LENGTH];
    int i;

    printf("Welcome to use our DNS daemon service!\n");
    printf("Before start, we need you to login and get the hostname.\n");
    printf("Username:");
    scanf("%s",cuser);
    cps=getpass("Password:");
    MD5((unsigned char*)cps,strlen(cps),(unsigned char*)&digest);
    for(i=0;i<16;i++)
        sprintf(&md5ps[i*2],"%02x",(unsigned int)digest[i]);
    if(serverAction(cuser,md5ps,NULL))
    {
        if(mkdir("/dnsd/",S_IRWXU)==0)
            printf("Program directory created.\n");
        FILE *fd=fopen("/dnsd/.login","w");
        if(!fd)
            printf("[DNS Daemon] Write file ERROR!\n");
        fprintf(fd,"%s:%s",cuser,md5ps);
        printf("[DNS Daemon] Write login info to record.\n");
        fclose(fd);
    }

    printf("Your hostname is %s\n",hostname);

    return 0;
}

int host(int update)
{
    FILE *lfd=fopen("/dnsd/.login","r");
    if(!lfd) {
        login();
        return 0;
    }
    else {
        char tmp[55],*cuser,*cps;
        fscanf(lfd,"%s",tmp);
        cuser=strtok(tmp,":");
        cps=strtok(NULL,":");
        if(update==0)
            serverAction(cuser,cps,NULL);
        else
            serverAction(cuser,cps,nip);
    }
    fclose(lfd);
    return 0;
}

int read_last_ip()
{
    FILE *fd=fopen("/dnsd/.last_ip_record","r");
    if(!fd)
        return 0;
    else {
        fscanf(fd,"%s",rip);
        fclose(fd);
    }
    return 1;
}

int get_now_ip()
{
    //system("ifconfig | awk -F'[ :  ]+' '/Bcast/{print $4}'>>/tmp/now_tmp");
    system("curl -s icanhazip.com >>/tmp/now_tmp");
    FILE *nfd=fopen("/tmp/now_tmp","r");                          
    fscanf(nfd,"%s",nip);
    fclose(nfd);
    system("rm -rf /tmp/now_tmp");
    return 0;
}

int write_now_ip()
{
    FILE *fd=fopen("/dnsd/.last_ip_record","w");
    if(!fd)
        printf("[DNS Daemon] Write file ERROR!\n");
    fprintf(fd,"%s",nip);
    printf("[DNS Daemon] Write new IP to record.\n");
    fclose(fd);
    return 0;
}

int compare_ip()
{
    if(strcmp(rip,nip)!=0) {
        printf("[DNS Daemon] New IP found!\n");
        return 1;
    }
    return 0;
}

int main ()
{
    if(getuid()!=0) {
        printf("[DNS Daemon] This tool need ROOT permission!\n");
        return 0;
    }
    host(0);
    while(1)
    {
        get_now_ip();
        printf("[DNS Daemon] IP:%s\n",nip);
        if(read_last_ip()==0) { //When first Run or IP Log miss
            printf("[DNS Daemon] Last IP record not found!\n");
            write_now_ip();
            host(1);    //Run update;
        }
        else {
            if(compare_ip()==1) { //When New IP found
                write_now_ip();
                host(1);    //Run update
            }
        }
        sleep(2);
    }

    return 0;
}

