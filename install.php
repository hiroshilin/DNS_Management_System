<?php
error_reporting(0);
$configset=0;
$dbset=0;
if(file_exists("config.php")==TRUE) {
	$configset=1;
}
if(file_exists("db.sqlite3")==TRUE) {
	if(filesize("db.sqlite3")>0) {
		$dbset=1;
	}
}
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>DNS Management - Installation</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.css" />
<link rel="stylesheet" type="text/css" href="css/dns_custom.css" />
<script src="js/jquery-1.11.1.min.js"></script>
</head>
<?php
if($_POST['dname']) {
	if($_POST['dnskey']) {
		$fp=fopen('config.php','w');
		if(fwrite($fp,"<?php\n\$domain=\"".$_POST['dname']."\";\n\$nskey_path=\"".$_POST['dnskey']."\";\n?>")!=FALSE) { 
		$configset=1;
		}
	}
}

if($_POST['user']) {
	if($_POST['fname']) {
		if($_POST['pass']) {
			require('db.php');
			require('dmapi.php');
			$db->exec("CREATE TABLE IF NOT EXISTS pub_domain(
						id integer PRIMARY KEY,
						creator varchar(20) NOT NULL,
						type varchar(1) NOT NULL,
						hostname varchar(20) NOT NULL,
						ip varchar(16) NOT NULL
					  )");
			$db->exec("CREATE TABLE IF NOT EXISTS user_list(
					  id integer PRIMARY KEY,
					  name varchar(20) NOT NULL,
					  fullname varchar(20) NOT NULL,
					  password varchar(32) NOT NULL,
					  hostname varchar(20) NULL,
					  ip varchar(16) NULL,
					  admin tinyint(1) NOT NULL
					)");
			addUser($_POST['user'],$_POST['pass'],"admin",$_POST['fname'],1);
			$dbset=1;
		}
	}
}
?>
<body>
<div class="main" style="width:800px;">
	<h1 style="text-align:center;">DNS Management - Installation</h1>
    <?php 
		if($configset==0) { ?>
			<div id="configsetter" style="width:70%; margin:auto;">
            <h4>Step 1. 建立設定檔</h4>
            <hr>
            <p style="text-align:center; color:#F00;">提醒：請先將DNS目錄權限設定成757！</p>
            <form class="regi" method="post">
              <table class="table">
                  <tr>
                      <td style="text-align:right;">網域名稱 Domain name:</td>
                      <td><input style="width:250px;" type="text" name="dname" required></td>
                  </tr>
                  <tr>
                      <td style="text-align:right;">DNS 金鑰路徑:</td>
                      <td><input style="width:250px;" type="text" name="dnskey" required></td>
                  </tr>
                  <tr>
                      <td colspan="2" style="text-align:center;"><button class="btn btn-primary" type="submit">設定</button></td>
                  </tr>
              </table>
        	</form>
           </div>
		<?php } else { ?>
        <div id="configdone" style="width:70%; margin:auto;">
        	<h4>Step 1. 建立設定檔</h4>
           <hr>
        	<p style="text-align:center; color:#0C0;"><img alt="check" width="30" src="img/check.png">設定檔已建立完成！</p>
        </div>
       <?php }
		if($configset==1&&$dbset==0) { 
			 ?>
			<div id="dbsetter" style="width:70%; margin:auto;">
            <h4>Step 2. 資料庫設定</h4>
            <hr>
            <form class="regi" method="post">
              <caption>新增一名系統管理員：</caption>
              <table class="table">
                  <tr>
                      <td style="text-align:right;">使用者名稱 Username:</td>
                      <td><input style="width:150px;" type="text" name="user" required></td>
                  </tr>
                  <tr>
                      <td style="text-align:right;">全名 Fullname:</td>
                      <td><input style="width:150px;" type="text" name="fname" required></td>
                  </tr>
                  <tr>
                      <td style="text-align:right;">密碼 Password:</td>
                      <td><input style="width:150px;" type="password" name="pass" required></td>
                  </tr>
                  <tr>
                      <td colspan="2" style="text-align:center;"><button class="btn btn-primary" type="submit">設定</button></td>
                  </tr>
              </table>
        	</form>
           </div>
		<?php } else if($configset==1&&$dbset==1){ ?>
        <div id="dbdone" style="width:70%; margin:auto;">
        	<h4>Step 2. 資料庫設定</h4>
           <hr>
        	<p style="text-align:center; color:#0C0;"><img alt="check" width="30" src="img/check.png">資料庫已建立完成！</p>
        </div>
		<?php }
		if($configset==1&&$dbset==1) {?> 
           <p>&nbsp;</p>
			<p style="text-align:center; color:#F00;">為了安全因素考量，請於執行過安裝步驟後將DNS資料夾權限調整成755並移除此檔案(install.php）！</p>
           <p style="text-align:center;"><button class="btn btn-success" onClick="javascript:location.href='login.php'">繼續</button></p>
		<?php } ?>
</div>
&nbsp;
</body>
</html>