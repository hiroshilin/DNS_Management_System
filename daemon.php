<?php
	session_start();
	if($_SESSION['auth']!=true)
	{
		header('Location: permissiondeny.php');
		exit;
	}
	require('config.php');
?>
<!doctype html>
<html>
<head>

<meta charset="UTF-8">
<link rel="stylesheet" type="text/css" href="css/bootstrap.css" />
<link rel="stylesheet" type="text/css" href="css/dns_custom.css" />
<title>DDNS Daemon</title>
</head>

<body>
	<p style="text-align:right; padding-right:50px;color:#F5F5F5;">
    	Login:&nbsp;<?php echo $_SESSION['user'];?>&nbsp;&nbsp;
		<?php if($_SESSION['admin']==true) {?><a href="admpanel.php">[Admin Panel]</a><?php } ?>&nbsp;
        <a href="udomain.php">[Personal Domain]</a> &nbsp;
        <a href="logout.php">[Logout]</a>
   	</p>
   <div class="main">
   		<h1 style="text-align:center;">DDNS Daemon Download</h1>
		<h4 style="text-align:center;"><?php echo $domain; ?></h4>
        <table class="table" style="margin:auto;">
        	<tr>
            	<td style="text-align:center;"><a href="dist/daemon.c"><img src="img/code.png"></a><br>請按右鍵「另存目標」</td>
           </tr>
           <tr>
           	<td>Compatible for:<br>
            		<table><tr>
                    <td><img src="img/apple.png" width="40px"></td>
                    <td><img src="img/tux.png" width="40px"></td>
                    <td><img src="img/bsd.png" width="40px"></td>
                    <td><img src="img/rPi.png" width="40px"></td>
                  </tr></table>
               </td>
           </tr>
        </table>
   </div>
    <div class="main" style="padding:10px; line-height:1.8;">
        <h1 style="text-align:center;">DDNS Daemon Readme</h1>
        <h4 style="text-align:center;"><?php echo $domain; ?></h4>
        <p style="font-size:16px; color:#F90;">使用須知：</p>
        1.請先確認你已經有註冊一個hostname（可以從個人domain管理頁面確認）</font> <br />
        2.電腦需有可編譯C程式的環境，以及 <font color="#FF0000">libssl-dev libcurl4-openssl-dev</font> 的編譯函式庫</font> <br />
        3.hostname不可以超過20個字元<br />
        <p style="font-size:16px; color:#F90;">編譯程式：</p>
        1.編譯指令： <font color="#3333FF">gcc -o ddns_daemon daemon.c -lcrypto -lcurl</font><br />
        2.執行daemon： <font color="#3333FF">sudo ./ddns_daemon</font><br />  <br /> 
        <p style="font-size:16px; color:#F90;">開機自動執行：</p>
        <p style="font-size:16px; color:#F90;"><img src="img/tux.png" width="30px">Use Ubuntu as example:</p>
        請在 <font color="#3333FF">/etc/rc.local</font> 加上（加在exit;上方）：
        <pre><font color="#FF0000">screen -d -m 程式放置目錄/ddns_daemon</font></pre>
        <p style="font-size:16px; color:#F90;"><img src="img/apple.png" width="30px">Use Mac OSX as example:</p>
        請在 程式放置目錄 裡面建立一個 "runddns.command" 的shell script：
        <pre><font color="#FF0000">#! /bin/bash
sudo screen -d -m /dnsd/ddns_daemon</font></pre>
		 編輯後請打開系統偏好設定/使用者與群組，打開自己帳號的登入項目，並新增剛剛建立的shell script!
        <img src="img/macdemo.png">
        <p style="font-size:16px; color:#F90;">如果程式執行異常、想要登出原有帳號：</p>
        執行指令： <font color="#3333FF">rm -f /dnsd/.login /dnsd/.last_ip_record /dnsd/tmp_nsupdate</font>
    </div>
	<div class="main" style="margin-bottom:20px;">
    	<h1 style="text-align:center;">Update Logs</h1>
        <ul>
        	<li>2014.05.04 Update daemon manual page</li>
           <li>2014.04.30 Fix the add/update domain bug</li>
        	<li>2013.08.08 Change structure and release source code</li>
        	<li>2013.07.28 Change Daemon SQL connection to HTTP connection</li>          
        </ul>
    </div>
</body>
</html>
